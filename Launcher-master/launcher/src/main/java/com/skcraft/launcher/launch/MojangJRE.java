package com.skcraft.launcher.launch;

import com.skcraft.launcher.util.Environment;
import com.skcraft.launcher.util.Platform;
import lombok.extern.java.Log;

import java.io.File;
import java.util.logging.Level;

/**
 * @author dags <dags@dags.me>
 */

@Log
public class MojangJRE {

    public static File getJRE() {
        File runtime = getRuntimeDir();
        if (runtime == null) {
            return null;
        }

        File x64 = resolve(runtime, "jre-x64"); // 32bit platform? violins
        File jre = getLatestJRE(x64);
        if (jre == null) {
            return null;
        }

        log.log(Level.INFO, "Detected Mojang-provided JRE {0}", jre);

        if (!resolve(jre, "bin", "java").setExecutable(true)) {
            log.log(Level.WARNING, "Unable to make 'java' executable!");
        }

        if (!resolve(jre, "bin", "javaw").setExecutable(true)) {
            log.log(Level.WARNING, "Unable to make 'javaw' executable!");
        }

        return resolve(jre, "bin");
    }

    private static File getRuntimeDir() {
        if (Environment.getInstance().getPlatform() == Platform.MAC_OS_X) {
            return getMacRuntimeDir();
        }

        if (Environment.getInstance().getPlatform() == Platform.WINDOWS) {
            return getWindowsRuntimeDir();
        }

        // let's assume linux users know how to install java because fuck knows where the minecraft launcher ends up
        return null;
    }

    // usually 'C:/Program Files (x86)/Minecraft/runtime'
    private static File getWindowsRuntimeDir() {
        File mcDir = new File(System.getenv("PROGRAMFILES(X86)"), "Minecraft");
        if (!mcDir.exists()) {
            // shouldn't really ever happen but people do weird things
            mcDir = new File(System.getenv("PROGRAMFILES"), "Minecraft");
        }
        return resolve(mcDir, "runtime");
    }

    // usually '/Applications/Minecraft.app/Contents/runtime'
    // prioritise user-local applications ('~/Applications/'), fall back to global '/Applications'
    private static File getMacRuntimeDir() {
        File mcApp = resolve(getHome(), "Applications", "Minecraft.app");
        if (!mcApp.exists()) {
            mcApp = new File("/Applications", "Minecraft.app"); // more likely
        }
        return resolve(mcApp, "Contents", "runtime");
    }

    private static File getLatestJRE(File dir) {
        File[] jres = dir.listFiles();
        if (jres == null || jres.length == 0) {
            return null;
        }

        File latest = null;
        for (File jre : jres) {
            if (latest == null || jre.lastModified() > latest.lastModified()) {
                latest = jre;
            }
        }

        return latest;
    }

    private static File getHome() {
        return new File(System.getProperty("user.home"));
    }

    private static File resolve(File parent, String... path) {
        File file = parent;
        for (String s : path) {
            file = new File(file, s);
        }
        return file;
    }
}
